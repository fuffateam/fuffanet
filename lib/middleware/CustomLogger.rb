require 'logger'

module Fuffanet

#  Logger object, required for Rake::CommonLogger
class CustomLogger < Logger
  
  alias_method :write , :<<

  def initialize(logdir,logname)

    Dir.mkdir(logdir, 0700) unless Dir.exist?(logdir)
    
    @logdir=logdir
    
    @logfile = File.open(File.join(@logdir,logname),'a')
    
    @logfile.sync = true
    
    super(@logfile,'weekly') # ages the logfile weekly
    
    # self.level=Logger::DEBUG  # could DEBUG, ERROR, FATAL, INFO, UNKNOWN, WARN
    self.level=Logger::DEBUG

    # $stdout.reopen(@logfile)    
    # $stderr.reopen(@logfile)  
    
  end
  
end

end
