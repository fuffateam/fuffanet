require 'sinatra/base'
require 'sinatra/flash'

module Fuffanet
  
  module Web

    class Engine < Sinatra::Base
      
      # load configuration (set app_name, environment variables, etc...)
      puts "Endpoint loading config " if require ( File.expand_path('../config',__FILE__) )

      Dir["#{File.expand_path(root)}/{helpers,models,controllers}/init.rb"].each do |file|
        puts "=> Endpoint loading #{file} <=" if require file
      end

      register Sinatra::Flash

    end

  end
  
end
