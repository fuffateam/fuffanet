module Sinatra
  module Fuffanet
    
    module NotificationHelper
      def send_email(params)
        logger.debug "=> #{__method__.to_s}"

        options = { :address              => "#{settings.smtp_host}",
                    :port                 => "#{settings.smtp_port}",
                    :domain               => ENV['REMOTE_HOST'],
                    :user_name            => "#{settings.smtp_username}",
                    :password             => "#{settings.smtp_password}",
                    :authentication       => "#{settings.smtp_authentication}",
                    :enable_starttls_auto => true  }

        Mail.defaults do
          delivery_method :smtp, options
        end

        begin
          
          Mail.deliver do
            to params[:to]
            from params[:from]
            subject params[:subject]
            body params[:body]
            content_type 'text/html; charset=UTF-8'            
          end

          logger.info "Email sent."

        rescue Exception=>e
          logger.error "Error #{e}"
          exit 1
        end        
        
      end
    end

  end
end
