# coding: utf-8
require 'mail'
require 'bcrypt'
require 'yaml'

require_relative 'language'
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::LanguageHelper

require_relative 'secure'
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::SecureHelper

require_relative 'time'
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::TimeHelper

require_relative 'notification'
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::NotificationHelper

require_relative 'model'
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::ModelHelper

require_relative 'view'
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::ViewHelper::FormHelper
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::ViewHelper::CssJs
Fuffanet::Web::Engine.helpers Sinatra::Fuffanet::ViewHelper::Menu
