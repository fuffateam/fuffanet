module Sinatra
  module Fuffanet

    module ModelHelper
      
      def user_add(params)
        User.add(params)
      end

      def user_changepassword(params)
        User.save_password(params[:name],params[:password])
      end

      def user_destroy(params)
        logger.debug "---------- #{params}"
        User.destroy(params)
      end      

      def idea_add(params)
        Idea.add(params)
      end
      
    end

  end
end
