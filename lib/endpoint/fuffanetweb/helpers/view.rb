# coding: utf-8
module Sinatra
  module Fuffanet
    module ViewHelper
      
      module FormHelper

        def contact_idea_action

          @params= params

          input=params.map { |k,v|
            v
          }[0]

          title, type, description, email= input["title"], input["type"], input["description"], input["email"]

          if ( email.length==0 || !( email =~ /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/i ))

            flash[:idea] = gettext "view.generic.email_address_not_valid"
            false
          elsif title.length==0
            flash[:idea] = gettext "view.idea.title_empty"
            false              
          else

            new_idea=idea_add({title: title,
                               author: email,
                               :description=>description,
                               :type=>type,
                              })

            if new_idea
              
              send_email({ to: "#{settings.email_info}",
                           from: email,
                           subject: gettext("view.idea.email.subject",{
                                              from: email
                                            }),
                           body: description
                         })
              
              true
              
            else
              
              false
              
            end
          end
          
        end        

        def contact_help_action

          @params= params

          email=@params[:contact][:inputUsername]
          message=@params[:contact][:inputMessage]
          
          if ( email.length==0 || ! ( email =~ /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/i ) )
            flash[:contact] = gettext "view.generic.email_address_not_valid"
            false
          elsif message.length==0
            flash[:contact] = gettext "view.contact.message_not_valid"
            false              
          else
            
            send_email({ to: "#{settings.email_info}",
                         from: email,
                         subject: gettext("view.contact.email.subject",{
                                            from: email
                                          }),
                         body: message
                       })
            true
          end
          
        end

        def resetpassword_form_validator

          @params=params

          username=@params[:resetpassword][:inputUsername]
          if username.length==0
            flash[:resetpassword] = gettext("user.resetpassword_form.username_empty")
            false
          else

            user = User.first(name: username)

            if user

              to=user[:email]
              confirm=user[:confirm]
              
              send_email({to: to,
                          from: "#{settings.email_noreply}",
                          subject: gettext("user.resetpassword.email.subject"),
                          body: gettext("user.resetpassword.email.body",
                                        {username: username,
                                         "app_name"=>settings.app_name,
                                         "node"=>settings.node,
                                         "confirm"=>confirm
                                        })
                         })
            end
            
            true
            
          end
          
        end

        def changepassword_form_validator

          @params=params

          if @params[:changepassword][:newInputPassword].length==0
            flash[:changepassword] = gettext("user.changepassword_form.password_required")
            false
          elsif @params[:changepassword][:newInputPassword].length<6
            flash[:changepassword] = gettext("user.changepassword_form.password_too_short")
            false
          elsif @params[:changepassword][:newInputPassword]!=params[:changepassword][:inputPasswordConfirm]
            flash[:changepassword] = gettext("user.changepassword_form.password_mismatch")
            false
          else
            if user_changepassword({name: current_user,
                                    password: @params[:changepassword][:newInputPassword],
                                   })
              true
            else
              false
            end
          end
          
        end
        
        def deleteuser_form_validator

          @params=params

          logger.debug ">>>>>>Params: #{params}"

          username=@params[:deleteuser][:inputUsername]
          if username.length==0
            logger.debug gettext("user.deleteaccount_form.username_required")
            flash[:deleteuser] = gettext("user.deleteaccount_form.username_required")
            false
          else
            user_destroy username
          end
          
        end
        
        def form_validation
          
          registration=params["registration"]

          inputUsername=registration['inputUsername']
          inputEmail=registration['inputEmail']
          inputPassword=registration['inputPassword']
          inputPasswordConfirm=registration['inputPasswordConfirm']

          flash[:registration]=""
          if inputUsername.length==0
            flash[:registration] = gettext("user.registration_form.username_required")
            false
          elsif ( inputEmail.length==0 || !( inputEmail =~ /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/i ) )
            flash[:registration] = gettext("view.generic.email_address_not_valid")
            false
          elsif ( inputPassword.length<6)
            flash[:registration] = gettext("user.registration_form.password_too_short")
            false            
          elsif inputPassword!=inputPasswordConfirm
            flash[:registration] = gettext("user.registration_form.password_mismatch")
            false            
          elsif User.exists?(inputUsername)
            flash[:registration] = gettext("user.registration_form.username_exists")
            false
          else

            new_user=user_add({name: inputUsername,
                               email: inputEmail,
                               :role_name=>'reader',
                               :registered_at=>Time.now,
                               :password=>inputPassword,
                              })
            
            if new_user

              confirm_code=new_user[:confirm]
              
              send_email({to: "#{inputEmail}" ,
                          from: "#{settings.email_noreply}",
                          subject: gettext("user.registration.email.subject",{"app_name"=>settings.app_name}),
                          body: gettext("user.registration.email.body",
                                        {"username"=>inputUsername,
                                         "app_name"=>settings.app_name,
                                         "node"=>settings.node,
                                         "confirm"=>confirm_code,
                                        })
                         })

              true
            else
              false
            end

          end

        end
      end

      module CssJs

        def puts_css
          css=<<-'EOF'

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet" type="text/css"/>
     <link href="/font-awesome/css/font-awesome.css" rel="stylesheet" />
     <link href="/css/styles.css" rel="stylesheet" />
EOF
          
          css
        end

        def puts_js
          js=<<-'EOF'
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="/js/jquery.min.js"></script>
    <!-- <script src="js/jquery.min.js"></script> -->
	
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
EOF
          
          js
        end

      end

      module Menu

        def print_html_footer(language=detect_browser_language)
          
          case language
          when "it"
            
            footer=<<-EOF

 <foot class="text-center">
	  <div class="footer-above">
        <div class="container-fluid">
          <div class="row">
            <div class="footer-col col-md-2">
              <h3><i class="fa fa-map-marker fa-fw"></i>&nbsp;Dove</h3>
              <p>3481 #{settings.app_name} Valley<br>Beverly Hills, CA 90210</p>
            </div>

            <div class="footer-col col-md-2">
              <h3><i class="fa fa-envelope-o fa-fw"></i>&nbsp;<a href="/contact-form">Contattaci</a></h3>
              <h3><i class="fa fa-info fa-fw"></i><a href="/about">Chi siamo</a></h3>
            </div>

            <div class="footer-col col-md-2">
              <h3><i class="fa fa-newspaper-o fa-fw"></i>&nbsp;<a href="/news">Novita</a></h3>
              <h3><i class="fa fa-gavel fa-fw"></i><a href="/terms">Termini</a></h3>
            </div>

			<div class="footer-col col-md-2">
              <h3><i class="fa fa-users fa-fw"></i>&nbsp;Accounting</h3>
			  <div class="text-center">
				<span class="fa-stack fa-lg">
                  <a href="/registration-form">
					<i class="fa fa-circle-o fa-stack-2x"></i>
					<i class="fa fa-user fa-stack-1x"></i>
				  </a>
				</span>
				&nbsp;
				<span class="fa-stack fa-lg">
                  <a href="/deleteuser-form">
					<i class="fa fa-user fa-stack-1x"></i>
					<i class="fa fa-ban fa-stack-2x"></i>
				  </a>
				</span>
			  </div>
            </div>
			
            <div class="footer-col col-md-4">
              <h3>Social network</h3>
              <ul class="list-inline">
                <li>
                  <a href="http://www.facebook.com/Fuffanet-521978454659390/" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                </li>
                <li>
                  <a href="https://www.google-plus.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                </li>
                <li>
                  <a href="https://www.twitter.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                </li>
              </ul>
            </div>

          </div>
        </div>
      </div>
      <div class="footer-below">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
			  <p>© 2014, 2015, <a href="http://#{@env['HTTP_HOST']}/">The #{settings.app_name} Foundation</a></p>
			  <p> Modified: #{Time.now.strftime("%m/%d/%Y")}</p>
            </div>
          </div>
        </div>
      </div>
    </foot>
		
EOF

          when "en"
            footer=<<-EOF
    <foot class="text-center">
	  <div class="footer-above">
        <div class="container-fluid">
          <div class="row">
            <div class="footer-col col-md-2">
              <h3><i class="fa fa-map-marker fa-fw"></i>&nbsp;Location</h3>
              <p>3481 #{settings.app_name} Valley<br>Beverly Hills, CA 90210</p>
            </div>

            <div class="footer-col col-md-2">
              <h3><i class="fa fa-envelope-o fa-fw"></i>&nbsp;<a href="/contact-form">Contact us</a></h3>
              <h3><i class="fa fa-info fa-fw"></i><a href="/about">About us</a></h3>
            </div>

            <div class="footer-col col-md-2">
              <h3><i class="fa fa-newspaper-o fa-fw"></i>&nbsp;<a href="/news">Latest news</a></h3>
              <h3><i class="fa fa-gavel fa-fw"></i><a href="/terms">Terms</a></h3>
            </div>

			<div class="footer-col col-md-2">
              <h3><i class="fa fa-users fa-fw"></i>&nbsp;Accounting</h3>
			  <div class="text-center">
				<span class="fa-stack fa-lg">
                  <a href="/registration-form">
					<i class="fa fa-circle-o fa-stack-2x"></i>
					<i class="fa fa-user fa-stack-1x"></i>
				  </a>
				</span>
				&nbsp;
				<span class="fa-stack fa-lg">
                  <a href="/deleteuser-form">
					<i class="fa fa-user fa-stack-1x"></i>
					<i class="fa fa-ban fa-stack-2x"></i>
				  </a>
				</span>
			  </div>
            </div>
			
            <div class="footer-col col-md-4">
              <h3>Social network</h3>
              <ul class="list-inline">
               <li>
                  <a href="https://www.facebook.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                </li>
                <li>
                  <a href="https://www.google-plus.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                </li>
                <li>
                  <a href="https://www.twitter.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                </li>
              </ul>
            </div>

          </div>
        </div>
      </div>
      <div class="footer-below">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
			  <p>© 2014, 2015, <a href="http://#{@env['HTTP_HOST']}/">The #{settings.app_name} Foundation</a></p>
			  <p> Modified: #{Time.now.strftime("%m/%d/%Y")}</p>
            </div>
          </div>
        </div>
      </div>
    </foot>
EOF
            
          end
          
          footer          
        end # end print_html_footer

        
        def print_html_menu(language=detect_browser_language)

          case language

          when "en"
            
menu=<<-EOM

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav">
    <li><a href="/idea"><i class="fa fa-play fa-fw"></i> Play</a>
	<li class="dropdown">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-forward fa-fw"></i> Projects<span class="caret"></span></a>
	  <ul class="dropdown-menu">
		<li><a href="/idea">IDEA!</a></li>
		<li role="separator" class="divider"></li>
		<li><a href="/q-demo">Q-DEMO</a></li>
	  </ul>
	</li>
	<li class="dropdown">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pause fa-fw"></i> instruments<span class="caret"></span></a>
	  <ul class="dropdown-menu">
		<li><a href="/docs">documentation</a></li>
		<li><a href="/links">links</a></li>
   		<li><a href="/benchmarks">benchmarks</a></li>
	  </ul>
	</li>
  </ul>

EOM

if is_logged?             
  menu<<= <<EOM
  <ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span></span><span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"> Account</span><span class="caret"></span></a>
	  <ul class="dropdown-menu">
		<li><a href="/idea"><i class="fa fa-play fa-fw"></i> Play</a></li>
		<li role="separator" class="divider"></li>
		<li><a href="/account"><i class="fa fa-wrench fa-fw"></i> Settings</a></li>
		<li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
	  </ul>
	</li>
  </ul>
</div>
EOM
  
else

  menu<<= <<EOM
      <ul class="nav navbar-nav navbar-right">
       <li><a href="/login-form"><i class="fa fa-eject fa-fw"></i> Login</a>
      </ul>
EOM
end # if

          when "it"
            
menu=<<-EOM

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav">
    <li><a href="/idea"><i class="fa fa-play fa-fw"></i> Play</a>
	<li class="dropdown">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-forward fa-fw"></i> Progetti<span class="caret"></span></a>
	  <ul class="dropdown-menu">
		<li><a href="/idea">IDEA!</a></li>
		<li role="separator" class="divider"></li>
		<li><a href="/q-demo">Q-DEMO</a></li>
	  </ul>
	</li>
	<li class="dropdown">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pause fa-fw"></i> Strumenti<span class="caret"></span></a>
	  <ul class="dropdown-menu">
		<li><a href="/docs">documentazione</a></li>
		<li><a href="/links">links</a></li>
   		<li><a href="/benchmarks">benchmarks</a></li>
	  </ul>
	</li>
  </ul>

EOM

if is_logged?             
  menu<<= <<EOM
  <ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
	  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span></span><span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"></span> Account<span class="caret"></span></a>
	  <ul class="dropdown-menu">
		<li><a href="/idea"><i class="fa fa-play fa-fw"></i> Play</a></li>
		<li role="separator" class="divider"></li>
		<li><a href="/account"><i class="fa fa-wrench fa-fw"></i> Impostrazioni</a></li>
		<li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Esci</a></li>
	  </ul>
	</li>
  </ul>
</div>
EOM
  
else

  menu<<= <<EOM
      <ul class="nav navbar-nav navbar-right">
       <li><a href="/login-form"><i class="fa fa-eject fa-fw"></i> Accedi</a>
      </ul>

EOM
end # if

    menu
          
        end # case
      end # function
      end # module
    end
  end
end
