module Sinatra
  module Fuffanet
    module SecureHelper

      # Validate the confirm code
      def confirm_user(code)
        User.confirm(code)
      end

      # Return the new_clear_password
      def confirm_resetpassword(code)
        User.confirm_resetpassword(code)
      end
      
      def is_logged?
        logger.debug "=>is_logged? #{session[:logged]?true:false}<="
        session[:logged]
      end

      def is_admin?
        is_logged? && session[:username]=="admin"
      end    
      
      def current_user
        if is_logged?
          session[:username]||"guest"
        else
          "guest"
        end
      end

      # Simple authentication against database
      def authenticate!
        
        inputs=params["login"]

        authenticated=User.authenticate(inputs["inputUsername1"], inputs["inputPassword1"])

        if authenticated
          session[:username]=inputs["inputUsername1"]
          session[:logged]=true
        else
          flash[:msg] = "Login failed"
        end

      end

      # TODO: authorize access resource based on user role
      def authorize!
        halt 401 , "Not authorized" unless ( is_logged? && is_admin? )        
      end
      
    end    
  end
end
