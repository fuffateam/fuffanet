module Sinatra
  module Fuffanet
    module LanguageHelper

      DEFAULT_LANGUAGE="it"

      # Message translation
      def gettext(key=nil, place_holders=nil)

        messages = begin
                     YAML.load(File.open("#{settings.views}/messages.yml"))
                   rescue ArgumentError => e
                     logger.error "Could not parse YAML: #{e.message}"
                   end
        
        logger.debug "key: #{key}"
        
        if messages[key]
          
          logger.debug "#{messages[key]}"
          
          language=detect_browser_language
          if messages[key][language]
            message=messages[key][language]
          else
            message=messages[key][DEFAULT_LANGUAGE]
          end
        else # Message does not exists
          message="Message not found"
        end

        place_holder_replacer(message,place_holders).gsub(/\A"|"\Z/, '') # remove trailing|leading "
        
      end

      # Replace holders into msg string
      def place_holder_replacer(msg='',place_holders=nil)
        
        place_holders.each_key do |k|
          logger.debug "key: #{k}, value: #{place_holders[k]}"
          msg.gsub!("\#\{#{k}\}",place_holders[k])
        end unless place_holders.nil?
        
        msg
      end

      # Get available languages into views/pages folder
      def get_available_languages_folders

        Dir[settings.views+"/locale/*"].select { |fn|            
          File.directory?(fn) && File.basename(fn).length==2
        } . collect { |rn|
          File.basename(rn)
        }
        
      end

      # Detect user language
      # Defualts to DEFAULT_LANGUAGE if not found in http headers
      def detect_browser_language

        default_language=DEFAULT_LANGUAGE

        if @env["HTTP_ACCEPT_LANGUAGE"].nil? # curl browser
          default_language
        else
          
          language=@env["HTTP_ACCEPT_LANGUAGE"][0,2]
          if get_available_languages_folders.include?(language)
            language
          else
            logger.debug "Language #{language} not supported, defaults to #{default_language}"
            default_language
          end
          
        end
      end

      # load page
      def load_page(page,layout)
        erb escape_link(page) , :layout => File.join( "layouts" , layout ) . to_sym
      end

      # Generate link :'locale/it/some_page'
      # if locale/language/page
      # else i18n/page
      def escape_link(link,language=detect_browser_language)

        pagefile=""

        if File.exist?(File.join(settings.views, "locale" , language , link+".erb"))
          logger.debug "Page:#{link} , Locale page #{language} found"
          pagefile=File.join("locale" , language , link)
        else
          logger.debug "Page:#{link} , Locale page #{language} not found, defaults to i18n"
          pagefile=File.join("i18n" , link)
        end
        
        pagefile . to_sym
      end
      
    end
  end
end
