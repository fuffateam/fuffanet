module Sinatra
  module Fuffanet
    module TimeHelper
      def time_now
        Time.now
      end
    end
  end
end
