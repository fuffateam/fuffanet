require 'data_mapper'

module Fuffanet
  
  module Web

    class Engine

      configure do
        
        connection_string= %Q{mysql://#{settings.mysql_username}:#{settings.mysql_password}@#{settings.mysql_host}/#{settings.mysql_schema}}
        puts "database connection #{connection_string}"

        # DataMapper::Logger.new($stdout, :debug)
        # DataMapper::Logger.new(Fuffanet::CustomLogger.new(File.join(File.expand_path(settings.root,__FILE__),"logs"),"application.log"), :debug, '[SQL] ')
        DataMapper.logger=Fuffanet::CustomLogger.new(File.join(File.expand_path(settings.root,__FILE__),"logs"),"application.log")
        # DataMapper.logger.set_log(Fuffanet::CustomLogger.new(File.join(File.expand_path(settings.root,__FILE__),"logs"),"application.log"))

        if DataMapper.setup(:default, connection_string)
          require_relative 'ddl'
          puts "Data models loaded"
          require_relative 'user'
          require_relative 'idea'
          puts "Data access object loaded"
        end
        
      end
      
    end
  end
end
