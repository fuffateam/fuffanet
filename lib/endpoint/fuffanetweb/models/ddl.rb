require 'data_mapper'

DataMapper::Model.raise_on_save_failure = true

class User
  
  include DataMapper::Resource

  # property :id,         Serial
  property :name,  String , :key => true
  property :password,  String, :length => 128 , :required => true
  property :email,      String , :required => true
  property :registered_at , DateTime
  property :confirm,  String, :length => 48 , :required => true
  property :confirmed_at , DateTime

end

class Role
  
  include DataMapper::Resource

  property :name,  String , :key => true
  property :description,  Text
  
end

class User
  belongs_to :role
end

class Role
  has n, :users
end

class Idea

  include DataMapper::Resource

  property :id, Serial
  property :title, String, :length=>128, :required=>true
  property :author, String, :required => true
  property :type, Enum[ :task, :project ]
  property :description, Text
  property :raiseTime, DateTime, :default => lambda{ |p,s| DateTime.now}

end

DataMapper.finalize
puts "end finalize"

