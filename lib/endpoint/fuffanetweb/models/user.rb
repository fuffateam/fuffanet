require 'bcrypt'

class User

  public

  # Add new user
  def self.add(params=nil)
    if params
      user= User.create(
        name: params[:name],
        email: params[:email],
        role_name: params[:role_name],
        registered_at: params[:registered_at],
        confirm: generate_random_string,
        password: encrypt_password(params[:password])
      )
      
      user
    else
      nil
    end
  end
  
  # Secure authentication
  def self.authenticate(username, password)
    user = User.first(name: username, :confirmed_at.not => nil)
    if user && ( BCrypt::Password.new(user.password) == password )
      true
    else
      false
    end
  end
  
  # Change password
  def self.save_password(username,password)
    password_hashed = encrypt_password(password)
    user = User.first(name: username)
    if user
      user.update(:password => password_hashed)
    else
      false
    end
  end

  # Confirm user registration
  def self.confirm(code)
    user=User.first( :confirm => code )
    if user
      user.update(:confirmed_at=>Time.now)
    else
      false
    end
  end

  # Reset password
  def self.confirm_resetpassword(code)
    new_clear_password=generate_random_string(12)
    user=User.first( :confirm => code)
    if user
      user.update(:password=>encrypt_password(new_clear_password))
      if user.saved?
        new_clear_password
      else
        false
      end
    else
      false
    end
  end  
  
  def self.exists?(name)
    return User.first(name: name)
  end

  def self.destroy(name)
    return User.first(name: name).destroy
  end
  
  protected
  
  # Generate a random string
  def self.generate_random_string(len=48)
    rand(36**len).to_s(36)
  end

  # Encrypt password
  def self.encrypt_password(clear_password)
    BCrypt::Password.create(clear_password)
  end  
  
end
