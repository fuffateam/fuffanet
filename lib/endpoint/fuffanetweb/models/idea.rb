class Idea

  public

  # Add new idea
  def self.add(params=nil)

    pp params
    
    if params
      user= Idea.create(
        title: params[:title],
        author: params[:author],
        description: params[:description],
        type: params[:type].downcase.to_sym,
      )
      user
    else
      nil
    end
  end
  
end
