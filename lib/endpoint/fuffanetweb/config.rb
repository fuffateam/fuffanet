require_relative 'version'

module Fuffanet
  module Web
    class Engine

      configure do

        set(:app_name) { APP_NAME }
        set :run, false

        enable :logging
        # use Rack::CommonLogger,  Fuffanet::CustomLogger.new(File.join(File.expand_path(settings.root,__FILE__),"logs"),"application.log")


        use Rack::Session::Cookie, :key => 'rack.session',
            :path => '/',
            :expire_after => 86400,
            :secret => 'my_super_secret_token_HERE!!!!'   # change this to invalidate all sessions

        set :root, File.join(File.dirname(__FILE__))
        set :public_folder, Proc.new { File.join(root, "views/assets") }
        set :views, Proc.new { File.join(root, "views/pages") }

        puts "base configuration__________________"
        
      end

      configure :development do

        enable :show_exceptions,:dump_errors
        
        Dotenv.load (File.join(File.expand_path(root),'.env')) if require 'dotenv' # load .env file
        
        # set(:node) { 'http://localhost:9292/' }
        set(:node) { ENV['DEV_NODE_NAME'] }
        
        
        set(:mysql_username) { ENV['DEV_MYSQL_DB_USERNAME'] }
        set(:mysql_password) { ENV['DEV_MYSQL_DB_PASSWORD'] }
        set(:mysql_host) {ENV['DEV_MYSQL_DB_HOST'] }
        set(:mysql_schema) {ENV['DEV_MYSQL_DB_SCHEMA'] }

        set(:smtp_host) {ENV['DEV_SMTP_HOST']}
        set(:smtp_port) {ENV['DEV_SMTP_PORT']}
        set(:smtp_username) {ENV['DEV_SMTP_USERNAME']}
        set(:smtp_password) {ENV['DEV_SMTP_PASSWORD']}
        set(:smtp_authentication) {ENV['DEV_SMTP_AUTHENTICATION']}

        set(:email_news) {ENV['DEV_EMAIL_NEWS']}
        set(:email_info) {ENV['DEV_EMAIL_INFO']}
        set(:email_marketing) {ENV['DEV_EMAIL_MARKETING']}
        set(:email_admin) {ENV['DEV_EMAIL_ADMIN']}
        set(:email_noreply) {ENV['DEV_EMAIL_NOREPLY']}
 
        puts "end development configuration__________________"

      end

      configure :production do

        disable :show_exceptions,:dump_errors
        # enable :show_exceptions,:dump_errors

        # The following variables has been set via openshift action_hooks at pre_build time
        set(:node) { ENV['OPENSHIFT_NODE_NAME'] }

        set(:mysql_username) { ENV['OPENSHIFT_MYSQL_DB_USERNAME'] }
        set(:mysql_password) { ENV['OPENSHIFT_MYSQL_DB_PASSWORD'] }
        set(:mysql_host) {ENV['OPENSHIFT_MYSQL_DB_HOST'] }
        set(:mysql_schema) {ENV['OPENSHIFT_APP_NAME'] }
        
        set(:smtp_host) {ENV['OPENSHIFT_SMTP_HOST']}
        set(:smtp_port) {ENV['OPENSHIFT_SMTP_PORT']}
        set(:smtp_username) {ENV['OPENSHIFT_SMTP_USERNAME']}
        set(:smtp_password) {ENV['OPENSHIFT_SMTP_PASSWORD']}
        set(:smtp_authentication) {ENV['OPENSHIFT_SMTP_AUTHENTICATION']}

        set(:email_news) {ENV['OPENSHIFT_EMAIL_NEWS']}
        set(:email_info) {ENV['OPENSHIFT_EMAIL_INFO']}
        set(:email_marketing) {ENV['OPENSHIFT_EMAIL_MARKETING']}
        set(:email_admin) {ENV['OPENSHIFT_EMAIL_ADMIN']}
        set(:email_noreply) {ENV['OPENSHIFT_EMAIL_NOREPLY']}

        puts "end production configuration__________________"
        
      end
      
    end
    
  end
end
