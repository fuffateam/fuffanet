module Fuffanet
  
  module Web

    class Engine

      enable :inline_templates

      error_log=Fuffanet::CustomLogger.new(File.join(File.expand_path(settings.root,__FILE__),"logs"),"errors.log")
      application_log=Fuffanet::CustomLogger.new(File.join(File.expand_path(settings.root,__FILE__),"logs"),"application.log")

      before {
        env["rack.errors"]= error_log
        env["rack.logger"]= application_log
      }
      
      get '/' do
        @title = "Welcome to #{settings.app_name}"
        load_page "index" , "layout"
      end

      get '/logout' do
        logger.info "logged out"
        session.clear
        redirect to '/'
      end

      # Post method to perform authentication login
      post '/login' do

        authenticate!

        if is_logged?
          redirect to '/'
        else
          redirect to '/login-form'
        end
      end
      
      get %r{/(links|faq|tools|contact-form|about|benchmarks|support|news|login-form|registration-form|account|deleteuser-form|terms|resetpassword-form|q-demo|idea|docs)$} do
        dest= params[:captures][0]
        load_page dest , "layout"
      end

      post '/register-user' do

        @message=gettext("user.registration.completed")
        
        if ! form_validation
          logger.info "Registrazione avvenuta"
          redirect '/registration-form'
        else
          logger.info "Errore"
          load_page "response" , "layout"
        end
        
      end

      get '/confirm-subscribe/:id' do

        if ( params['id'].length==48 && (confirm_user(params['id'])  ))
          @message=gettext("user.registration.confirmed")
        else
          @message=gettext("user.registration.unconfirmed")
        end

        load_page "response" , "layout"
        
      end

      get '/confirm-resetpassword/:id' do

        if ( params['id'].length==48 )
          new_clear_password=confirm_resetpassword(params['id'])
          if new_clear_password
            @message=gettext("user.resetpassword.confirmed",{new_password: new_clear_password})
          end          
        else
          @message=gettext("user.registration.unconfirmed")
        end

        load_page "response" , "layout"
        
      end

      post '/account/changepassword' do
        
        @message=gettext("user.changepassword.completed")

        if ! changepassword_form_validator
          redirect '/account'
        else
          load_page "response" , "layout"          
        end
        
      end

      post '/account/delete' do

        @message=gettext("view.user.delete.completed")

        check_form= deleteuser_form_validator
        logger.debug "check_form: #{check_form}"
        if ! check_form
          redirect "deleteuser-form"
        else
          load_page "response" , "layout"
        end
        
      end      

      post '/account/resetpassword' do

        @message=gettext("user.resetpassword.completed")

        if ! resetpassword_form_validator
          redirect '/resetpassword-form'
        else
          load_page "response" , "layout"          
        end
        
      end

      post '/contact/help' do
        
        @message=gettext("view.contact.help.submitted")
        if ! contact_help_action
          redirect '/contact-form'
        else
          load_page "response" , "layout"          
        end

      end

      post '/contact/idea' do
        
        @message=gettext("view.idea.submitted")

        if ! contact_idea_action
          redirect '/idea'
        else
          load_page "response" , "layout"
        end
        
      end      

      get '/admin' do
        authorize!
        load_page "admin" , "layout"        
      end

      not_found do
        @title="Error 404 #{settings.app_name}"
        logger.error "#{@env}"
        logger.error "#{@title}"
        load_page "not_found" , "message_layout"
      end

      error do
        @error = env['sinatra.error']
        logger.error "#{@error}"
        "Errore in corso..."
      end

    end
    
  end
  
end

# __END__

# @@ en/success
#       <h4><pre><%= @message %></pre></h4>
