namespace :fuffanet do

  task :load_environment do
    require File.join(File.dirname(__FILE__),'..','main')
  end

  task :name => 'fuffanet:load_environment' do
    puts "Name: #{Fuffanet::Web::Engine.app_name}"
  end

  task :node => 'fuffanet:load_environment' do
    puts "Node: #{Fuffanet::Web::Engine.node}"
  end
  
  task :age => 'fuffanet:load_environment' do
    puts "Age: #{Fuffanet::VERSION}"
  end

  task :place => 'fuffanet:load_environment' do
    puts "Environment: #{Fuffanet::Web::Engine.environment}"
  end

  desc "Who am i"
  task :whoami => [:name,:age,:place,:node]

end
