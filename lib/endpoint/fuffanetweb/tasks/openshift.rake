# coding: utf-8
#
# Rake task to prepare openshift environment.
# The following enviroment override .env local variable
#

namespace :fuffanet do
  namespace :openshift do

    desc "Delete openshift app."
    task :"app_delete" => [:prompt] do
      
      cmd="rhc app-delete"
      cmd<<=" #{ENV['OPENSHIFT_GEAR']} "
      cmd<<=" --namespace #{ENV['OPENSHIFT_DOMAIN']} "
      cmd<<=" -l#{ENV['OPENSHIFT_LOGIN']} " unless  ENV['OPENSHIFT_LOGIN'].gsub(/\s+/,"")==""
      
      system "#{cmd}"
      
    end       

    desc "List openshift apps."
    task :"apps" => [:prompt_login] do

      cmd="rhc apps"
      cmd<<=" -l#{ENV['OPENSHIFT_LOGIN']} " unless  ENV['OPENSHIFT_LOGIN'].gsub(/\s+/,"")==""
      
      system "#{cmd}"
      
    end    
    
    desc "Switch the openshift deployment branch."
    task :"switch_deploy_branch" => [:prompt] do
      

      cmd="rhc app-configure "
      cmd<<=" #{ENV['OPENSHIFT_GEAR']} "
      cmd<<=" --deployment-branch #{ENV['OPENSHIFT_GEAR']} "
      cmd<<=" -l#{ENV['OPENSHIFT_LOGIN']} " unless  ENV['OPENSHIFT_LOGIN'].gsub(/\s+/,"")==""
      
      system "#{cmd}"
      
    end

    desc "Show the openshift deployment branch."
    task :"show_deploy_branch" => [:prompt] do

      cmd="rhc show-app "
      cmd<<=" #{ENV['OPENSHIFT_GEAR']} "
      cmd<<=" --configuration "
      cmd<<=" --namespace #{ENV['OPENSHIFT_DOMAIN']} "
      cmd<<=" -l#{ENV['OPENSHIFT_LOGIN']} " unless  ENV['OPENSHIFT_LOGIN'].gsub(/\s+/,"")==""
      
      system "#{cmd}"
      
    end    

    desc "List all user-defined environment variables set on the application."
    task :"list_env" => :prompt do

      puts "List environment on openshift cloud for node #{ENV['OPENSHIFT_GEAR']} and domain #{ENV['OPENSHIFT_DOMAIN']} using account #{ENV['OPENSHIFT_LOGIN']}"

      cmd=" rhc env-list "
      cmd<<=" #{ENV['OPENSHIFT_GEAR']} "
      cmd<<=" --namespace #{ENV['OPENSHIFT_DOMAIN']} "
      cmd<<=" -l#{ENV['OPENSHIFT_LOGIN']} " unless  ENV['OPENSHIFT_LOGIN'].gsub(/\s+/,"")==""
      
      system "#{cmd}"
    end

    desc "Enable port forwarding, connect to remote services on local port."
    task :"enable_port_forwarding" => :prompt do

      cmd="rhc port-forward  "
      cmd<<=" #{ENV['OPENSHIFT_GEAR']} "
      cmd<<=" --namespace #{ENV['OPENSHIFT_DOMAIN']} "
      cmd<<=" -l#{ENV['OPENSHIFT_LOGIN']} " unless  ENV['OPENSHIFT_LOGIN'].gsub(/\s+/,"")==""

      system "#{cmd}"
      
    end
    
    task :prompt_gear do

      STDOUT.print "Please insert openshift gear name (es. demo19): "
      ENV['OPENSHIFT_GEAR']=STDIN.gets.strip # TODO: required
      
    end
    
    task :prompt_domain do

      STDOUT.print "Please insert openshift domain (es. cksmile): "
      ENV['OPENSHIFT_DOMAIN']=STDIN.gets.strip # TODO: required

    end

    task :prompt_login do

      STDOUT.print "Please insert openshift account login(calife@fuffanet.com or leave empty): "
      ENV['OPENSHIFT_LOGIN']=STDIN.gets.strip # TODO: not required or check_valid_email if not empty

    end
    
    task :prompt do

      STDOUT.puts "Your are going to deploy a new application on the openshift cloud."
      STDOUT.puts "Please, provide the information required."

      ["fuffanet:openshift:prompt_gear",
       "fuffanet:openshift:prompt_domain",
       "fuffanet:openshift:prompt_login"].each do |t|
        Rake::Task[t].reenable
        Rake::Task[t].invoke
      end
      
    end
    
  end
end
