namespace :fuffanet do
  namespace :database do
    
    task :prompt => 'fuffanet:load_environment' do |t,args|
      cmd=%Q{ mysql -u#{Fuffanet::Web::Engine.mysql_username} -h#{Fuffanet::Web::Engine.mysql_host} -p#{Fuffanet::Web::Engine.mysql_password} -D#{Fuffanet::Web::Engine.mysql_schema} }
      puts "#{cmd}"
      system "#{cmd}"
      puts "End task #{t.name}"
    end      

    desc "mysql prompt"
    task :mysql_prompt => :prompt
    
    desc "Drop the database"
    task :drop => 'fuffanet:load_environment' do |t,args|
      cmd=%Q{ echo "drop database if exists #{Fuffanet::Web::Engine.mysql_schema};" | mysql -u#{Fuffanet::Web::Engine.mysql_username} -h#{Fuffanet::Web::Engine.mysql_host} -p#{Fuffanet::Web::Engine.mysql_password} }
      puts "#{cmd}"
      system "#{cmd}"
      puts "End task #{t.name}"
    end

    task :create => 'fuffanet:load_environment' do |t,args|
      cmd=%Q{ echo "create database if not exists #{Fuffanet::Web::Engine.mysql_schema};" | mysql -u#{Fuffanet::Web::Engine.mysql_username} -h#{Fuffanet::Web::Engine.mysql_host} -p#{Fuffanet::Web::Engine.mysql_password} }
      system "#{cmd}"
      puts "End task #{t.name}"      
    end

    # desc "Performs an automigration (resets your db data, leaving an empty database)"
    task :auto_migrate => 'fuffanet:load_environment' do |t,args|
      if defined?(DataMapper)
        DataMapper.auto_migrate!
        puts "End task #{t.name}"      
      end      
    end

    desc "Generate the schema if not exists.Performs a non destructive automigration"
    task :auto_upgrade => 'fuffanet:load_environment' do |t,args|
      if defined?(DataMapper)
        DataMapper.auto_upgrade!
        puts "End task #{t.name}" 
      end        
    end

    desc "Drop, create the database and migrates from scratch, leaving an empty database"
    task :clear_schema=>[:drop,:create,:auto_migrate]

    desc "Drop, create the database, migrate and initialize with seed data"
    task :seed_data=>[:clear_schema] do |t,args|
      require File.join(File.dirname(__FILE__),'../models/seed')
      puts "End task #{t.name}" 
    end
    
  end
end
