namespace :fuffanet do

  namespace :bench do

    CONC_LEVEL=25
    REQ_NUM=500

    desc "HTML ab benchmark"
    task :"html" => 'fuffanet:load_environment' do
      system "ab -r -n #{REQ_NUM} -c #{CONC_LEVEL} -w #{Fuffanet::Web::Engine.node}"
    end

    desc "GNUPLOT ab benchmark"
    task :"tsv" => 'fuffanet:load_environment' do
      system "ab -r -n #{REQ_NUM} -c #{CONC_LEVEL} -g benchmark.tsv #{Fuffanet::Web::Engine.node}"
    end
    
  end
  
end
