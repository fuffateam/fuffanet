
Following the rack definitions, we are used to distinguish among middleware and endpoint.

A Rack endpoint is just an application that adheres to the Rack spec.

A Rack middleware is any software component/library which assists with but is not directly involved in the execution of some task.

 {appbase}/lib:
  total used in directorye 16 available 1805172
  drwxr-xr-x 2 mpucci 1000 4096 Nov  4 16:21 endpoint
  drwxr-xr-x 2 mpucci 1000 4096 Nov  4 16:19 middleware
