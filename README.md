## **Synopsis** ##

Welcome to Fuffanet. We are the ones who brought Silicon Valley to Rome.

## **Motivation** ##

Fuffanet...sounds like another music.

## **Installation and local setup** ##

```
#!bash

git clone https://fuffanet@bitbucket.org/fuffateam/fuffanet.git
```

```
#!bash

cd fuffanet && rackup
```

## **API Reference** ##

* [Openshift](https://developers.openshift.com/)
* [Rack](http://rack.github.io/)
* [Phusion Passenger](https://www.phusionpassenger.com/library/walkthroughs/start/)
* [Sinatra](http://www.sinatrarb.com/)
* [Bundler](http://bundler.io/)

## **Setup node on openshift cloud** ##

Deploy Fuffanet node on openshift cloud.

```
#!bash

./bin/clone_openshift.sh
```
## **Contributors** ##

* calife@fuffanet.com
* brydo@fuffanet.com

## **License** ##

Do what you want License

## **Wishes** ##

Good luck

## **Author** ##

calife
