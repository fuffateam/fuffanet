
# require the middlewares
Dir["#{File.dirname(__FILE__)}/lib/middleware/*.rb"].each do |file|
  puts "require #{file}"
  require file
end

# require the endpoints
Dir["#{File.dirname(__FILE__)}/lib/endpoint/**/main.rb"].each do |file|
  puts "require #{file}"
  require file
end

app = Rack::Builder.app do # Rack::Builder implements a DSL
  
  use Rack::Reloader, 1 # Only reload ruby files
  use Rack::Deflater # Compress
  use Rack::CommonLogger, Fuffanet::CustomLogger.new(File.join(File.dirname(__FILE__),"logs"),"requests.log") # use verb add a middleware to the stack
  use Rack::ShowExceptions

  run Fuffanet::Web::Engine # dispatch to an applicationf
  
end
 
run app # HERE THE WORLD BEGIN!
