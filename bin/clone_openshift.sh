#!/bin/bash
#
#########################################################################################################################################
# HOWTO: HOW TO deploy fuffanet to a new openshift application.
#
# Requirements:
# 1) bitbucket account to fuffanet repository
# 2) new openshift account to deploy fuffanet application
# 3) a mysql account granted to create the $NODE_NAME database
#
# Wednesday, 03. February 2016
#########################################################################################################################################

GIT_REPOS="https://fuffanet@bitbucket.org/fuffateam/fuffanet.git"

echo -n  "Openshift setup"

echo -n  "Application Name(es. sinpol):"
read NODE_NAME

echo -n  "Account Login(es. calife@fuffanet.com):"
read OPENSHIFT_ACCOUNT

echo -n "Are you sure you want to create the application name $NODE_NAME? (yes|no) > "
read confirm

if [ "$confirm" == "yes" ]; then

	APP_DIR=$HOME/$NODE_NAME
	
	echo -n "[TASK1]> Create the openshift application sourced on bitbucket repository (yes|no) > "
	read confirm
	if [ "$confirm" == "yes" ]; then
		rhc app create ruby-1.9 mysql-5.5 -a $NODE_NAME --repo $APP_DIR --from-code "$GIT_REPOS" -l$OPENSHIFT_ACCOUNT

		echo "cd into $APP_DIR"
		cd $APP_DIR && pwd

		echo -n "[TASK2]> Create new local branch $NODE_NAME and push to bitbucket (yes|no) > "
		read confirm
		if [ "$confirm" == "yes" ]; then
			git checkout -b  $NODE_NAME && git push -u upstream $NODE_NAME

			echo -n "[TASK3]> Switch openshift deployment branch to $NODE_NAME  (yes|no) > "
			read confirm
			if [ "$confirm" == "yes" ]; then
				rhc app-configure $NODE_NAME --deployment-branch $NODE_NAME -l$OPENSHIFT_ACCOUNT

				echo -n "[TASK4]> Run a build and Deploy application $NODE_NAME on remote openshift (yes|no) > "
				read confirm
				if [ "$confirm" == "yes" ]; then
					git push origin $NODE_NAME

					echo "[END]>"
					echo "--------------------------------------------------------------------"
					echo "1) Remote openshift application configured.                         "
					echo "2) Local application deployed. cd $APP_DIR && rackup                "
					echo "--------------------------------------------------------------------"
					
					exit 0

				fi #TASK4
				
			fi #TASK3
			
		fi #TASK2
		
	fi # TASK1

fi # START

exit 1
